# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import io
import sys
import socket
import requests
from unittest import TestCase

from hamcrest import is_, is_not, contains_string

from commodity.os_ import SubProcess, PIPE
from commodity.testing import assert_that, wait_that
from commodity.net import localhost, listen_port


class EchoTest(TestCase):
    def setUp(self):
        assert_that(localhost, is_not(listen_port(2000)))
        self.out = io.BytesIO()
        self.err = io.BytesIO()
        server = SubProcess('ncat -l -p 2000 -e /bin/cat', stdout=self.out, stderr=self.err)
        wait_that(localhost, listen_port(2000))
        self.addCleanup(self.check, server)
        self.addCleanup(server.terminate)

    def test_ok(self):
        s = socket.socket()
        s.connect(('localhost', 2000))
        s.send(b'hello')
        answer = s.recv(100)
        assert_that(answer, is_(b'hello'))

    def test_fail(self):
        s = socket.socket()
        s.connect(('localhost', 2000))
        s.send(b'hello')
        answer = s.recv(100)
        assert_that(answer, is_(b'bye'))

    def test_2(self):
        self.fail()
        
    def was_ok(self):
        def list2reason(exc_list):
            if exc_list and exc_list[-1][0] is self:
                return exc_list[-1][1]     

        if hasattr(self, '_outcome'):  # Python 3.4+
            result = self.defaultTestResult()  # These two methods have no side effects
            self._feedErrorsToResult(result, self._outcome.errors)
        else:  # Python 3.2 - 3.3 or 3.0 - 3.1 and 2.7
            result = getattr(self, '_outcomeForDoCleanups', self._resultForDoCleanups)
        error = list2reason(result.errors)
        failure = list2reason(result.failures)
        return not error and not failure

    def check(self, ps):
        if self.was_ok():
            return

        print("\n", ps)
        print("stdout:\n", self.out.getvalue().decode())
        print("stderr:\n", self.err.getvalue().decode())
        

class WebServerTest(TestCase):
    def setUp(self):
        assert_that(localhost, is_not(listen_port(8000)))
        server = SubProcess('python -m SimpleHTTPServer 8000', stdout=PIPE, stderr=PIPE)
        wait_that(localhost, listen_port(8000))
        self.addCleanup(server.terminate)

    def test_client(self):
        r = requests.get('http://localhost:8000')
        assert_that(r.status_code, is_(200))
        assert_that(r.text, contains_string(os.path.basename(__file__)))
